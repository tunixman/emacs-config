;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(package-initialize)

(random t)

;; (setq
;;  frame-resize-pixelwise t
;;  minibuffer-frame-alist '((name . "*Minibuffer*")
;;                           (title . "*Minibuffer*")
;;                           (height . 2)
;;                           (fullscreen . fullwidth)
;;                           (menu-bar-lines)
;;                           (tool-bar-lines)
;;                           (vertical-scroll-bars)
;;                           (horizontal-scroll-bars)
;;                           (left-fringe)
;;                           (right-fringe)
;;                           (minibuffer . only)
;;                           (unsplittable . t)
;;                           (user-position . t)
;;                           (user-size . t)
;;                           )
;;  initial-frame-alist '((height . 40)
;;                        (width . 100)
;;                        (menu-bar-lines . 1)
;;                        (tool-bar-lines . 1)
;;                        (minibuffer)
;;                        (unsplittable . t)
;;                        (vertical-scroll-bars . right)
;;                        (left-fringe . 3)
;;                        (right-fringe . 3)
;;                        (horizontal-scroll-bars)
;;                        (user-size t)
;;                        (user-position t)
;;                        )
;;  default-frame-alist initial-frame-alist
;;  )

(defun tum-custom-mkdirs (s v)
  "Create the directory `v', then `set-default' `s' to `v'.
This won't update the default value if it's already `v', but will
always try to create the directory."

  (set-default s v)

  (unless (file-accessible-directory-p v)
    (mkdir v t)
    )
  )

(defun tum-custom-f-touch (s v)
  "Touch the file `v' or create it otherwise.
Similar to `tum-custom-mkdirs', this will call `f-touch' on `v',
and only set the default of `s' if `v' differs. It will also try
and create the parent directory in case it's a subdirectory of
`tum-biblio-base'"

  (set-default s v)

  (let ((d (file-name-directory v)))
    (when d
      (unless (file-accessible-directory-p d)
        (mkdir d t)))
    (unless (file-exists-p v)
      (f-touch v)
      )
    )
  )

(defgroup tum-emacs nil "Customization for The UNIX Man Emacs"
  :group 'emacs)

(defcustom tum-emacs-license "GPL-3"
  "The license to insert."
  :group 'tum-emacs
  :type 'string
  )

(defcustom tum-emacs-header-padding 15
  "The padding for header fields."
  :group 'tum-emacs
  :type 'integer
  )

(defcustom tum-emacs-maintainer
  (format "%s <%s>" "Evan Cofsky" "evan@theunixman.com")
  "The maintainer."
  :group 'tum-emacs
  :type 'string
  )

(defgroup tum-emacs-python nil "Python configuration for Emacs processes."
  :group 'tum-emacs)

(defcustom tum-emacs-python-home
  "~/.emacs.d/tum.d/python/"
  "Python virtualenv home for any Emacs python processes."
  :group 'tum-emacs-python
  :type 'directory)

(defun tum-emacs-python-home-subdir (d)
  "Return `d' as a subdirectory of `tum-python-home'."

  (concat
   (file-name-as-directory
    (substitute-in-file-name tum-emacs-python-home))
   (file-name-as-directory d))
  )

(defcustom tum-emacs-python-bin-dir
  (tum-emacs-python-home-subdir "bin")
  "The bin path of the Emacs python virtualenv"
  :group 'tum-emacs-python
  :type 'directory)

(defun tum-emacs-python-bin-file (f)
  "Return a full file path to `f' under `tum-emacs-python-bin-dir'."

  (concat tum-emacs-python-bin-dir (file-name-nondirectory f))
  )

(defcustom tum-emacs-python-program
  (tum-emacs-python-bin-file "python3")
  "The python executable"
  :group 'tum-emacs-python
  :type 'file)

(defgroup tum-emacs-js nil
  "Configuration for JavaScript."
  :group 'tum-emacs
  )

(defcustom tum-emacs-js-global-dir (file-name-as-directory "/opt/npm-packages/")
  "Root of the NPM global installation."
  :group 'tum-emacs-js
  :type 'directory
  )

(defcustom tum-emacs-js-global-bin-dir
  (file-name-as-directory (concat (substitute-in-file-name tum-emacs-js-global-dir) "bin"))
  "The path to global NPM executables."
  :group 'tum-emacs-js
  :type 'directory
  )

(defconst tum-init-dir "~/.emacs.d/init.d/"
    "Path to all of the `req-package' initialization files.")

(defconst tum-custom-el "~/.emacs.d/custom/custom.el"
    "Path to the custom file.")

(defconst tum-local-root "~/.emacs.d/tum.d/"
  "Path to installed elisp modules.")

(defconst emacs-run-data-dir "~/.emacs.d/run/"
    "Root directory for all runtime files. This will be expanded
    and turned into a true directory name, and then used by
    `emacs-run-data-dir' to calculate a module runtime path.")

(defun emacs-run-data-dir (module component)
    "Return a directory for the runtime files of a module's component.

If the path doesn't exist it will be created with 0700 modes."

    (let* ((run-dir-truename (expand-file-name emacs-run-data-dir))
           (module-component-path
            (concat run-dir-truename
                    (file-name-as-directory module)
                    (file-name-as-directory component)))
           )

        (make-directory module-component-path t)
        (set-file-modes module-component-path #o700)
        module-component-path
        )
    )

(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(setq vc-handled-backends nil)

(setq auto-save-list-file-prefix (emacs-run-data-dir "auto-save" "list"))
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)

(load tum-custom-el)

(require 'package)

(mapc
 (lambda (d) (add-to-list 'load-path d))
 (directory-files tum-local-root t "^[^.]"))

(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

(package-install 'use-package t)
(require 'use-package)

(defun append-after-init-hook-f (f)
  "Append `f' to the `after-init-hook'."

  (add-hook 'after-init-hook f t)
  )


(defun require-bind-key ()
  "Require `bind-key'."
  (require 'bind-key)
  )
(append-after-init-hook-f #'require-bind-key)

(defun init-load-init-dir ()
  "Load `load-dir' and then load the init files from `tum-init-dir'"

  (use-package load-dir
    :demand t
    :ensure t
    :init
    (setq
     force-load-messages t
     load-dir-debug t
     load-dir-recursive t
     load-dirs '("~/.emacs.d/init.d/"))
    :commands (load-dir-one)
    :config
    (load-dir-one tum-init-dir)
    )
  )
(append-after-init-hook-f #'init-load-init-dir)

(defun misandry-init ()
  "Finish initializing misandry."

  (when (display-graphic-p)
    (load-theme 'Misandry-Light)
    (powerline-center-theme)
    ;;(tum-frames-mode)
    )

  (exec-path-from-shell-initialize)
  (add-to-list 'exec-path tum-emacs-js-global-bin-dir)

  (server-force-delete)
  (server-start)

  (global-unset-key (kbd "C-z"))
  (global-unset-key (kbd "C-x C-z"))
  )
(append-after-init-hook-f #'misandry-init)

(defvar jacqueline-mode nil "Whether we're in jacqueline-mode or not.")

(defun jacqueline-mode ()
  "Turn on big default face if Jacqueline is on."
  (interactive)

  (if jacqueline-mode
      (progn
        (setq jacqueline-mode nil)
        (set-face-attribute 'default nil :height 100)
        (toggle-frame-maximized))

    (setq jacqueline-mode t)
    (set-face-attribute 'default nil :height 180)
    )
  )

(global-set-key (kbd "s-<f10>") #'jacqueline-mode)
