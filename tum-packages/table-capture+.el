;;; table-capture+.el --- Capture and uncapture textual tables automatically.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This uses `table-capture' to automatically identify a region of
;; text as a table. Once done editing, it will then call
;; `table-release', and then remove the table layout
;; characters and clean up the whitespace.

;;; Code:

(require 'table)

(defvar table-capture+-delims "[|,[:blank:]]"
  "The set of possible column delimiters.
`table-capture+-delim' uses this to find the delimiter to use for
`table-capture', and `table-capture+-release' then uses it to
restore the table to it's delimited state.")

(defvar-local table-capture+-delim nil
  "Contains the original cell delimiter for a captured table.")

(defun table-capture+-delim (beg end)
  "Finds what the delimiter character is for the current region.
This will check the current row for any of the default delimiter characters and store"
  (interactive "r")

  (when table-capture+-delim
    (error "Buffer already has a captured table.")
    )
  (save-mark-and-excursion
    (beginning-of-line)
    (if (re-search-forward table-capture+-delims-re (end-of-line))
        (setq table-capture+delim (char-after))
      (signal 'search-failed "Could not find any of `table-capture+-delims'")
      )
    )
  )

(defun table-capture+-text (beg end)
  "Converts space, tab, comma, or pipe-separated columns to a table.
This uses `table-capture' to capture the region, and when
finished, `table-capture+-release' can be used to return the text
to its original delimited format."

  (interactive "r")

  (table-capture+-delim)
  (table-capture beg end table-capture+-delims "\n" 'left 12)
  )

(defun table-capture+--clean (beg end)
  "Replace multiple spaces with table-capture+-delim and clean up double lines.
If `table-capture+-delim' is a space, though, leave non-terminal whitespace alone."

  (save-mark-and-excursion
    ()
    (unless (string= " " table-capture+-delim)
      (while (re-search-forward " +" end t)
        (replace-match table-capture+-delim)
        )
      )

  )
(defun table-capture+-release ()
  "Finds the extent of the current table and releases it.
This also cleans up the whitespace left behind."
  (interactive)

  ;;; Save the old point, then get the table bounds.
  (let ((old-point (point-marker))
        (ul-corner (table-goto-top-left-corner))
        (br-corner (table-goto-bottom-right-corner))
        )

    ;;; Release the current table.
    (table-release)

    (if (string= table-capture+-delim " ")
        ())
  )
(provide 'table-capture+)
;;; table-capture+.el ends here
