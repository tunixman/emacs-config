;;; tum-frames.el --- Frame-per-buffer management with buffer actions.
;;; -*- lexical-binding: t; -*-

;; Copyright (C) 2017, 2018 Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: frames
;; Version: 2018011101
;; Package-Requires: ((emacs "24"))
;; URL: https://gitlab.com/misandrist/emacs-config

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This provides frame-based windows instead of windows in a single
;; frame. It's sort of like `one-on-one', but uses `display-buffer'
;; actions.
;;
;; Buffers with patters matching `tum-frames-special-buffers' will be
;; displayed with `display-buffer-reuse-window', then
;; `display-buffer-in-previous-window', and finally
;; `display-buffer-pop-up-frame'. We allow the same window and
;; switching frames, and also reusing frames.
;;
;; All others, we try and create a new frame with a new window, since
;; it's common to want the same file in multiple windows while
;; editing.
;;
;; This also rebinds
;;

;;; Code:

(require 'frame)

;;; Customization group and general parameters.

(defgroup tum-frames nil "The UNIX Man Frames Configuration."
  :group 'frames)

(defcustom tum-frames-special-alist
  '((left . (- 0))
    (top . (- 0))
    (user-position . t)
    (user-size . t)
    (auto-raise . t)
    (auto-lower . t)
    (minibuffer)
    (unsplittable . t)
    (vertical-scroll-bars . right)
    (left-fringe . 5)
    (right-fringe . 5)
    (horizontal-scroll-bars)
    (user-position . t)
    (user-size . t)
    )
  "The frame alist for sepecial buffers.
Buffer names matching `tum-frames-special-buffers' get these
  `frame-parameters'. They're displayed on the top right of the
  screen, aren't splittable, have vertical scroll bars on the
  right, no horizontal scroll bars, and no minibuffer."

  :group
  'tum-frames

  :type '(alist :value-type 'sexp)
  )

(defcustom tum-frames-special-buffers
  '("^\\*Help.*\\*$"
    "^\\*[Ii]nfo.*\\*$"
    "^\\*[hH]elm.*\\*$"
    "^\\*.*[Cc]ompil.*\\*"
    "^\\*.*[Mm]agit.*"
    "^\\*Messages\\*"
    "^\\*[Tt]heme.*"
    "^\\*scratch\\*"
    "^\\*.+"
    )

  "Regular expressions matching special buffer names.
These are matched against the `buffer-name' being created, and if
any match, the frame is special."

  :group 'tum-frames
  :type '(repeat :tag "Buffer Name Pattern"
                 :value-type 'string)
  )

(defun tum-frames-special-pattern-p (name)
  "Returns `t' if `name' matches `tum-frames-special-buffers'.
This works by using the comparator argument to `seq-contains'
over `tum-frames-special-buffers' on the buffer name, so we check
explicitly for `nil' instead of just not `t'.
"

  (seq-contains
   tum-frames-special-buffers
   name
   '(lambda (p n) (not (eq nil (string-match p n))))
   )
  )



(defcustom tum-frames-special-buffers-functions
  '(tum-frames-special-pattern-p)

  "A list of functions to see if a buffer is special.
Each function should take a string, the buffer name, and return
`t' if the buffer is special, or `nil' otherwise."


  :group 'tum-frames
  :type '(repeat function)
  )

(defun tum-frames-special-buffer-p (name)
  "Calls each `tum-frames-special-buffers-functions'.
If any return `t', the buffer is special."

  (if (run-hook-with-args-until-success 'tum-frames-special-buffers-functions name)
      t
    nil)
  )


;; Actions to further customize special buffers on creation.

(defcustom tum-frames-setup-special-buffers-functions-alist nil
  "Patterns and functions to run on frames matching them.
This `alist' is checked for a matching entry, and if one is
found, each function is called on the newly-created frame and the
specific buffer. This allows finer-tuning on top of the different
`display-buffer' behavior. Functions should be added with
`tum-frames-setup-special-buffers-functions-add'.

Function signature:

    (defun f (frame buffer) ... )"

  :group 'tum-frames
  :type '(alist :key-type string :value-type (list function))
  )

(defun tum-frames-setup-special-buffers-functions-add (p f)
  "Add `f' to the list for pattern `p'.
See `tum-frames-setup-special-buffers-functions-alist' for details."

  (let*
      ((fs (assq p tum-frames-setup-special-buffers-functions-alist))
       (e (cons p (append (list f) fs)))
       (dl (assq-delete-all p tum-frames-setup-special-buffers-functions-alist))
       )

    (add-to-list 'dl e)
    (setq tum-frames-setup-special-buffers-functions-alist dl)
    )
  )

(defun tum-frames-setup-special-buffers-functions-run (frame)
  "Run all of the `tum-frames-special-buffers-functions' for `frame'.
This is run after creating a frame if it's a special frame, and
runs only the functions matching the frame name."

  ; Get the buffer and the buffer's name.
  (let* ((b (car (frame-parameter
                 frame 'buffer-list))) ; The buffer itself
         (bn (buffer-name b)) ; The buffer name
         )

    (seq-map
     (lambda (f)
       (f frame))
     (assoc-default
      bn tum-frames-setup-special-buffers-functions-alist
      '(lambda (p n) (not (eq nil (string-match p n))))
      )
     )
    )
  )

(defun tum-frames-menu-tool-bar (frame buffer)
  "Disable menu and tool bars for special frames.
`tum-frames-init' adds this to
`tum-frames-special-buffers-functions-alist' as a catch-all."

  (set-frame-parameter frame "menu-bar-lines" 0)
  (set-frame-parameter frame "menu-bar-lines" 0)
  )

(defun tum-frames-hide-frame (frame)
  "Hide a frame rather than delete or iconify it."

  (set-frame-parameter frame 'visibility nil)
  )

(defun tum-frames-hide-window (&optional window)
  "Hides the frame of `window', or of `selected-window'."

  (tum-frames-hide-frame
   (window-frame
    (if (windowp window) window (selected-window))))
  )


(defun tum-frames-init ()
  "Initialize `tum-frames-mode'.
This first adds `tum-frames-special-buffers-functions-run' to the
`after-make-frame-functions' list. This enables our additional
customization functions to run.

Next, it adds tum-frames-menu-tool-bar to the catch-all pattern
of `tum-frames-special-buffers-functions-alist'.

For general configuration:

- We set the auto-hide function in general to `hide-frame' so
that redisplaying frames is fast.

- We set up `display-buffer-alist' to call
`tum-frames-special-buffer-p' first."

  (tum-frames-setup-special-buffers-functions-add ".*" #'tum-frames-menu-tool-bar)
  ;; (add-to-list 'after-make-frame-functions #'tum-frames-setup-special-buffers-functions-run)

  (setq
   frame-auto-hide-function #'iconify-frame
   display-buffer-alist
   '((#'tum-frames-special-buffer-p
      (display-buffer-reuse-window
       display-buffer-in-previous-window
       display-buffer-pop-up-frame)
      (inhibit-same-window . nil)
      (inhibit-switch-frame . nil)
      (reusable-frames . t)
      (pop-up-frame-parameters . tum-frames-special-alist)
      )
     (".*"
      (display-buffer-reuse-window
       display-buffer-in-previous-window
       display-buffer-pop-up-frame)
      (inhibit-same-window . nil)
      (inhibit-switch-frame . nil)
      (reusable-frames . t)
      )
     )
   )

  ;; Override `find-file' with with `find-file-other-frame'.
  (advice-add 'find-file :override #'find-file-other-frame)

  ;; Override `delete-window' with `delete-frame'.
  (advice-add 'delete-window :override #'delete-frame)

  ;; Override `switch-to-buffer' with `switch-to-buffer-other-frame'.
  (advice-add 'switch-to-buffer :override #'switch-to-buffer-other-frame)

  ;; Wrap `quit-window' with `delete-frame'
  (advice-add 'quit-window :override #'delete-frame)

  ;; Wrap `delete-other-windows' with `delete-other-frames'
  (advice-add 'delete-other-windows :override #'delete-other-frames)

  ;; Wrap `display-buffer-pop-up-window' with `display-buffer'.
  (advice-add 'display-buffer-pop-up-window :around
              (lambda (orig-fun buffer alist)
                (display-buffer-same-window buffer nil)))

  ;; Every window is dedicated.
  (advice-add 'set-window-dedicated-p :around
              (lambda (orig-fun &rest args) t))
  )

;;;###autoload
(define-minor-mode tum-frames-mode
  "A minor mode that uses frames instead of windows."
  :init-value nil
  :group 'tum-frames
  :global t
  :require 'tum-frames
  )

(add-hook 'tum-frames-mode-hook #'tum-frames-init)

(provide 'tum-frames)
;;; tum-frames.el ends here
