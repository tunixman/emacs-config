;;; lentic-rstdoc.el --- rstdoc support for lentic         -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: docs, files, languages, tools, maint

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This defines a `lentic-chunk-configuration' for modes that use
;; `rst-mode' comments. Currently we include:
;;
;; - `cmake-mode'

;;; Code:

;; #+begin_src emacs-lisp
(require 'lentic-chunk)

;; ** `cmake-mode'

(defun lentic-cmake-rst-oset (conf)
  "The `cmake-mode' block comments for `lentic-commented-chunk-configuration'"
  (lentic-m-oset
   conf
   :this-buffer (current-buffer)
   :comment "# "
   :comment-start "#[[.rst:"
   :comment-stop "#]]"
   )
  )

(defun lentic-cmake-new ()
  "Configure a new `lentic-commented-chunk-configuration' for `cmake-mode'.

Give it the extension '.rst'."

  (lentic-cmake-rst-oset
   (lentic-commented-chunk-configuration
    "cm-rst"
    :lentic-file
    (concat (file-name-sans-extension (buffer-file-name) ".rst"))
    )
   )
  )

;;;###autoload
(defun lentic-cmake-rst-init ()
  (lentic-cmake-new)
  )

(add-to-list 'lentic-init-functions
             'lentic-cmake-rst-init)

(provide 'lentic-cmake)

;;; lentic-cmake.el ends here
