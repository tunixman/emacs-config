;;; tum-lentic-configurations.el --- Some `lentic-mode` configurations.  -*- lexical-binding: t; -*-

;;; Header:

;; Copyright (C) 2017 Evan Cofsky
;; Author: Evan Cofsky <evan@theunixman.com>
;; Version: 0.1
;; Package-Requires:  (('lentic . "20161202.1352")('rst-mode . "1.326")('cmake-mode . "20160928.505"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This includes some `lentic-mode' configurations for various modes,
;; like `cmake-mode', to handle the various styles of comments
;; separately from the code itself.

;;; Code:

;; ** Require Lentic Configurations

;; #+begin_src emacs-lisp
(require 'tum-lentic-configurations-cmake)

(provide 'tum-lentic-configurations)
;;; lentic-configurations.el ends here
