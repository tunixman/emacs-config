;;; haskell-module.el --- Skeletons for Haskell modules.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky(defun format-pair (a b) <evan@theunixman.com>
;; Keywords: abbrev, languages, local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


(defvar haskell-default-license "GPL-3"
  "The default license to insert that can be overridden locally.")

(defvar haskell-default-header-padding 15
  "The default padding for header fields.")

(defvar haskell-default-maintainer
  (format "%s <%s>" user-full-name user-mail-address)
  "The default Maintainer.")

(defun format-header (a b &optional padding)
    "Format a header comment `a': {`p' spaces} `b' with padding of `p' spaces.

The default padding is `haskell-default-header-padding'"

    (let* ((p (if padding padding 15))
           (f (format "%%-%ds: %%s" p))
           (k (capitalize a)))

      (format f k b)
      )
    )

(define-skeleton haskell-module-skeleton
  "The header component of a Haskell module."

  nil
  "{-|" \n
  (format-pair "Module" (file-name-base (buffer-file-name))) \n
  (format-pair "Description:" (setq str (skeleton-read "Brief Description: "))) \n
  (format-pair "Copyright" (concat "©" (format-time-string "%Y"))) \n
  (format-pair "License" haskell-default-license) \n
  (format-pair "Maintainer"  haskell-default-maintainer) \n
  (format-pair "Stability" "experimental") \n
  (format-pair "Portability" "POSIX") \n
  \n
  "Description:" \n
  \n
  "-}"
  \n

  "module ." (file-name-base (buffer-file-name)) "where" \n
  )

(define-skeleton haskell-test-module-skeleton
  "Inserts the boilerplate for a QuickCheck module."

  nil
  "{-# LANGUAGE TemplateHaskell #-} -- for 'testGroupGenerator'" \n
  "{-# OPTIONS_GHC -Wno-orphans #-} -- for 'Arbitrary' instances" \n
  \n

  (haskell-module-skeleton)

  "import Test.Framework" \n
  "import Test.Framework.TH" \n
  "import Test.Framework.Providers.QuickCheck2 (testProperty)" \n
  "import Test.QuickCheck" \n
  \n

  _

  \n
  \n

  "properties ∷ Test" \n
  "properties = $(testGroupGenerator)" \n
  )

(provide 'haskell-module)
;;; haskell-module.el ends here
