(require 'use-package)

;; (defvar tum-purescript-ide-next-port nil
;;   "The next port to use for `psc-ide-server-start'.")

;; (defvar tum-purescript-ide-servers nil
;;   "An alist mapping project roots to ports.
;; This maps `projectile-project-root's to `psc-ide-port's. If one
;; isn't found, a new one is assigned and `psc-ide-server-start' is
;; called with the new port.")

;; (defun tum-projectile-psc-ide-server-start ()
;;   "This will call `psc-ide-server-start' in `projectile-project-root'.
;; It checks to see that one isn't running first, and only then will start a server.

;; The checks are:

;; 1. Check for an entry in `tum-purescript-ide-servers'
;;    If found, ")

(use-package psc-ide-mode
  :ensure t
  :init
  (setq psc-ide-use-npm-bin t)
  (add-hook 'purescript-mode-hook #'psc-ide-mode)
  :commands (psc-ide-mode)
  )

(use-package purescript-decl-scan-mode
  :ensure t
  :init
  (setq purescript-decl-scan-add-to-menubar t)
  (add-hook 'purescript-mode-hook #'turn-on-purescript-decl-scan)
  :commands (turn-on-purescript-decl-scan)
  )

(use-package purescript-mode
  :ensure t
  :mode "\\.purs\\'"
  :commands (purescript-mode)
  :bind (:map purescript-mode-map
              ("C-," . purescript-move-nested-left)
              ("C-." . purescript-move-nested-right)
              )
  :init
  (add-hook 'purescript-mode-hook #'turn-on-purescript-indentation)
  )


(provide 'init-purescript)
