;;; init-java.el --- Configure the Java Environment.  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: tools, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'use-package)

(use-package jdee
  :ensure t
  :init
  (setq jdee-launch-beanshell-on-demand-p nil
        jdee-flycheck-enable-p nil
        )
  :commands
  (jdee-mode)
  :mode
  ("\\.java'" . jdee-mode)
  )

(provide 'init-java)
;;; init-java.el ends here
