;;; init-projectile.el --- Projectile configuration and loading.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'use-package)

(defvar projectile-add-ignored-files
  '("*TAGS" "*~" "cabal.sandbox.config" "*.min.*" "*.map"
    "*.log" "*.git*" "*#*" ".*" "cabal.project.local")

  "Add these files to `projectile-globally-ignored-files'"
  )

(defvar projectile-add-ignored-directories
  '(".idea" ".eunit" ".git" ".hg" ".fslckout"
    ".bzr" "_darcs" ".tox" ".svn" ".stack-work"
    ".psci_modules" "cabal-sandbox" ".cabal-sandbox"
    "bower_components" "node_modules" "package"
    "output" "dist" "build" "site" "venv" ".venv"
    "dist" "dist-newstyle"
    )

  "Add these files to `projectile-globally-ignored-directories'"
  )

(defvar projectile-add-ignored-suffixes
  '(".hi" ".xz")

  "Add these to `projectile-globally-ignored-file-suffixes'")

(defun project-container (root)
  "Returns the parent directory only of `root'"

  (file-name-nondirectory
   (directory-file-name
    (file-name-directory
     (directory-file-name root)
     )
    )
   )
  )

(use-package projectile
  :ensure t
  :init (setq
         projectile-cache-file
         (concat
          (emacs-run-data-dir "projectile" "cache")
          "projectile.cache")

         projectile-known-projects-file
         (concat
          (emacs-run-data-dir "projectile" "known-projects")
          "projectile-bookmarks.eld")

         projectile-indexing-method 'alien
         projectile-completion-system 'helm
         projectile-enable-idle-timer nil
         projectile-tags-backend 'ggtags
         projectile-verbose nil
         projectile-enable-caching t
         projectile-file-exists-local-cache-expire (* 5 60)
         projectile-file-exists-remote-cache-expire (* 10 60)
         projectile-sort-order 'recentf
         projectile-switch-project-action #'projectile-dired
         )
  :bind (("<XF86HomePage>" . projectile-speedbar-open-current-buffer-in-tree))
  :config
  (setq
   projectile-mode-line '(:eval
                          (condition-case nil
                              (format " P:%s (%s)"
                                      (project-container (projectile-project-root))
                                      (projectile-project-name))
                            (error "")
                            )
                          )
   projectile-globally-ignored-directories
   (delete-dups
    (append projectile-globally-ignored-directories
            projectile-add-ignored-directories))

   projectile-globally-ignored-files
   (delete-dups
    (append projectile-globally-ignored-files
            projectile-add-ignored-files))

   projectile-globally-ignored-file-suffixes
   (delete-dups
    (append
     projectile-globally-ignored-file-suffixes
     projectile-add-ignored-suffixes))
   )

  (projectile-register-project-type
   'projectile-cabal-new
   '("cabal.project" "*.cabal" "cabal.project.local")
   :compile "cabal new-build"
   )

  (projectile-mode)
  :demand
  )

(use-package helm-projectile
  :ensure manual
  :commands helm-projectile-on

  :init
  (setq
   helm-projectile-fuzzy-match t
   helm-projectile-virtual-dired-remote-enable t
   )
  (add-hook 'projectile-mode-hook #'helm-projectile-on)
  )

(use-package projectile-speedbar
  :ensure manual
  :init (setq projectile-speedbar-projectile-speedbar-enable t
              projectile-speedbar-frame-mode nil)
  :commands (projectile-speedbar-open-current-buffer-in-tree projectile-speedbar-toggle)
  )

(provide 'init-projectile)
;;; init-projectile.el ends here
