;;; init-autocomplete.el --- Initialize the Ace package       -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, local, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'use-package)

(use-package ac-helm
  :ensure t
  )

;; (use-package ac-clang
;;   :ensure t
;;   :commands (ac-clang-initialize ac-clang-activate-after-modify)
;;   :init
;;   (add-hook 'c-mode-common-hook #'ac-clang-activate-after-modify)
;;   :config
;;   (ac-clang-initialize)
;;   )

;; (use-package ac-ctags
;;   :ensure t
;;   )

(use-package ac-html
  :ensure t
  )

(provide 'init-autocomplete)

;;; init-autocomplete.el ends here
