;;; init-latex.el --- Configure various LaTeX packages.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: tex, local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'use-package)

(use-package latex-math-preview
  :ensure t
  :commands (latex-math-preview-expression
             latex-math-preview-insert-symbol
             latex-math-preview-insert-mathematical-symbol
             latex-math-preview-insert-text-symbol)
  )

(use-package latex-unicode-math-mode
  :ensure t
  :init
  (add-hook 'TeX-mode-hook #'latex-unicode-math-mode)
  :commands (latex-unicode-mode latex-unicode-math-mode)
  )

; AucTeX configuration

(use-package preview-latex
  :ensure auctex
  :init
  (setq preview-image-type 'tiff)
  (add-hook 'TeX-mode-hook #'LaTeX-preview-setup)
  :commands (LaTeX-preview-setup)
  )

(use-package tex-site
  :ensure auctex
  :init
  (setq TeX-engine 'luatex
        TeX-auto-save t
        TeX-parse-self t)
  (setq-default TeX-master nil)
  )

(use-package parsebib
  :ensure t
  :demand
  )

(use-package bibtex
  :ensure t
  :init
  (setq
   bibtex-file-path ".:/projects/misandrist/Papers/"
   bibtex-completion-bibliography 'tum-papers-bibliography
   bibtex-completion-library-path 'tum-papers-archive
   bibtex-completion-notes-path 'tum-papers-notes
   bibtex-completion-additional-search-fields '(tags keywords journal booktitle)
   bibtex-completion-display-formats
    '((article       . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${journal:40}")
      (inbook        . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
      (incollection  . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
      (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
      (t             . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*}"))
    bibtex-completion-pdf-symbol "⌘"
    bibtex-completion-notes-symbol "✎"
    bibtex-completion-pdf-open-function (lambda (fpath)
                                          (call-process "xdg-open" nil 0 nil fpath))
    )
  )

(use-package reftex
  :ensure t
  :init
  (add-hook 'LaTeX-mode-hook #'reftex-mode)
  (setq reftex-plug-into-AUCTeX t)
  :bind (:map LaTeX-mode-map
              (("M-l" . reftex-citation)
               )
              )
  :commands (reftex-mode
             reftex-end-of-bib-entry
             reftex-parse-bibtex-entry
             reftex-citation
             reftex-reset-scanning-information)
  )

(provide 'init-latex)
;;; init-latex.el ends here
