;;; init-wakatime.el --- Configure wakatime          -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: unix, calendar, convenience, outlines

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Configure

;;; Code:

(require 'use-package)

;; (use-package wakatime
;;   :ensure t
;;   :init
;;   (setq
;;    wakatime-python-bin tum-emacs-python-program
;;    wakatime-cli-path (tum-emacs-python-bin-file "wakatime")
;;    )
;;   :after (exec-path-from-shell)
;;   :commands (global-wakatime-mode)
;;   :config
;;   (global-wakatime-mode)
;;   :demand
;;   )

(provide 'init-wakatime)
;;; init-wakatime.el ends here
