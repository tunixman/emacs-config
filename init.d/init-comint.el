(require 'use-package)

(setq compilation-environment '("TERM=xterm-256color"))

(use-package prog-mode
    :mode
    ("\\.zsh$" . shell-script-mode)
    ("zsh" . shell-script-mode)
    )

(use-package term+
  :ensure t
  :commands (term+setup)
  )

(use-package comint
  :init
  (add-hook 'comint-preoutput-filter-functions #'xterm-color-filter)
  )

(use-package esh-mode
  :config
  (add-to-list 'eshell-preoutput-filter-functions #'xterm-color-filter)
  (setq eshell-output-filter-functions
        (remove 'eshell-handle-ansi-color eshell-output-filter-functions)
        )
  )

(use-package eshell
  :after (esh-mode)
  :commands (eshell)
  :init
  (setq eshell-directory-name (emacs-run-data-dir "eshell" "control"))
  (add-hook
   'eshell-mode-hook
   (lambda ()
     (setq xterm-color-preserve-properties t)
     )
   )
  )

(use-package better-shell
  :commands (better-shell-shell better-shell-remote-open better-shell-sudo-here)
  :init
  (setq explicit-shell-file-name "/bin/zsh")
  :demand
  )

(use-package compile
  :commands (compilation-filter)
  :config
  (defun xterm-color-compilation-start (proc)
    "Differentiate between `compilation-mode' and `comint' buffers.
If the `process-filter' of `proc' is `compilation-filter', this
is a process associated with a compilation-mode buffer.  We call
`xterm-color-filter' before its own filter function."
    (when (eq (process-filter proc) 'compilation-filter)
      (set-process-filter proc
                          (lambda (proc string)
                            (funcall 'compilation-filter proc
                                     (xterm-color-filter string))))
    )
  )
  (add-hook
   'compilation-start-hook
   (lambda (proc)
     ;; We need to differentiate between compilation-mode buffers
     ;; and running as part of comint (which at this point we assume
     ;; has been configured separately for xterm-color)
     (when (eq (process-filter proc) 'compilation-filter)
       ;; This is a process associated with a compilation-mode buffer.
       ;; We may call `xterm-color-filter' before its own filter function.
       (set-process-filter
        proc
        (lambda (proc string)
          (funcall 'compilation-filter proc
                   (xterm-color-filter string)
                   )
          )
        )
       )
     )
   )
  )

(use-package xterm-color
  :ensure t
  :commands (xterm-color-compilation-start xterm-color-filter xterm-color-preserve-properties)
  :init
  (progn
    (setq comint-output-filter-functions
          (remove 'ansi-color-process-output comint-output-filter-functions)
          )
    )
  :config
  (term+setup)
  )


(provide 'init-comint)
