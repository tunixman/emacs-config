;;; init-markdown.el --- Configuration for Markdown Modes  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, hypermedia, tools, calendar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Configure the various markdown modes.

;;; Code:

(require 'use-package)

(defvar pandoc-markdown-extns
  '("table_captions" "simple_tables" "multiline_tables" "grid_tables" "pipe_tables")
  "Extensions to add to pandoc markdown.")

(defvar pandoc-markdown-command
  (mapconcat
   #'shell-quote-argument
   (list "pandoc"
         (mapconcat
          (lambda (a) a)
          (cons "--read=markdown" pandoc-markdown-extns) "+")
         "--mathjax"
         )
   " ")
  )

(use-package markdown-mode
  :ensure t
  :after markdown-toc
  :commands (markdown-mode markdown-live-preview-window-eww)
  :init
  (setq markdown-asymmetric-header t
        markdown-coding-system "utf-8"
        markdown-coding-system (quote utf-8)
        markdown-command pandoc-markdown-command
        markdown-command-needs-filename t
        markdown-enable-math t
        markdown-enable-prefix-prompts t
        markdown-enable-wiki-links t
        markdown-fontify-code-blocks-natively t
        markdown-footnote-location 'immediately
        markdown-hide-markup t
        markdown-hide-urls t
        markdown-hr-display-char "⎯"
        markdown-indent-on-enter 'indent-and-new-item
        markdown-live-preview-delete-export 'delete-on-destroy
        markdown-live-preview-window-function #'markdown-live-preview-window-eww
        markdown-marginalize-headers t
        markdown-nested-imenu-heading-index t
        markdown-reference-location 'immediately
        markdown-url-compose-char "⚓"
        markdown-use-pandoc-style-yaml-metadata t
        markdown-wiki-link-fontify-missing t
        markdown-wiki-link-search-parent-directories t
        markdown-wiki-link-search-subdirectories t

        )
  :mode "\\.md" "\\.mdwn"
  )

(use-package markdown-toc
  :commands markdown-toc-generate-toc
  :init
  (add-hook 'markdown-mode-hook
            (lambda ()
              (add-hook 'before-save-hook (lambda ()
                                            (when (not git-commit-mode)
                                              (markdown-toc-generate-toc)))
                        nil t)
              )
            )
  :bind (:map markdown-mode-map
              ("C-c C-t c" . markdown-toc-generate-toc)
              )
  :ensure t)

(provide 'init-markdown)
;;; init-markdown.el ends here
