;;; init-magit.el --- Configure Magit                -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; First, we do some configuration for frames, for helm, and a few
;;; other things.
;;

;;; Code:

(require 'use-package)

(use-package magit
  :ensure t
  :commands (magit-wip-after-apply-mode
             magit-wip-after-save-mode
             magit-wip-before-change-mode
             magit-auto-revert-mode
             magit-status)
  :init
  (setq
   ;; Branching

   ;;; Use the remote upstream if possible
   magit-branch-prefer-remote-upstream t
   ;;; Read the remote upstream first
   magit-branch-read-upstream-first t

   auto-revert-use-notify t
   magit-auto-revert-immediately t

   magit-diff-use-overlays t

   magit-display-buffer-function #'magit-display-buffer-traditional
   magit-pre-display-buffer-hook #'magit-save-window-configuration
   magit-bury-buffer-function #'magit-restore-window-configuration

   ;; magit-display-buffer-function #'display-buffer
   ;; magit-pre-display-buffer-hook #'current-frame-configuration
   ;; magit-bury-buffer-function #'set-frame-configuration
   magit-buffer-name-format "*%M%v: %t*"
   magit-save-repository-buffers 'dontask

   ;;; Use Helm
   magit-completing-read-function #'helm--completing-read-default

   ;;; Don't display lighters for wip modes.
   magit-wip-after-apply-mode-lighter ""
   magit-wip-after-save-local-mode-lighter ""
   magit-wip-before-change-mode-lighter ""
   )

  :bind (("M-<XF86MyComputer>" . magit-status))

  ;; Really load this.
  :demand
  :config
  ;; Enable all of the WIP modes and `magit-auto-revert-mode'
  ;; immediately.
  (magit-auto-revert-mode)
  (magit-wip-after-save-mode)
  (magit-wip-after-apply-mode)
  (magit-wip-before-change-mode)
  )

(use-package git-commit
  :ensure t
  :pin melpa
  :init
  ;; Make sure we're using `markdown-mode' for
  ;; `git-commit-major-mode'.
  (setq git-commit-major-mode #'markdown-mode)
  :commands (git-commit-mode global-git-commit-mode)
  )

(provide 'init-magit)
;;; init-magit.el ends here
