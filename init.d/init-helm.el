(require 'use-package)

(use-package helm
  :ensure t
  :pin manual
  :after async popup helm-config
  :demand
  :init
  (setq
   helm-always-two-windows nil
   helm-apropos-fuzzy-match t
   helm-bookmark-show-location t
   helm-buffer-max-length 140
   helm-buffers-end-truncated-string "…"
   helm-buffers-favorite-modes '(haskell-mode
                                 purescript-mode
                                 lisp-interaction-mode
                                 emacs-lisp-mode
                                 text-mode
                                 org-mode
                                 markdown-mode)
   helm-buffers-fuzzy-matching t
   helm-candidate-number-limit 150
   helm-candidate-separator "――――――――"
   helm-completion-in-region-fuzzy-match t
   helm-completion-in-region-display-function #'helm-display-buffer-in-own-frame
   helm-default-external-file-browser "dolphin"
   helm-display-buffer-height 40
   helm-display-buffer-width 80
   helm-display-buffer-reuse-frame t
   helm-display-function #'helm-display-buffer-in-own-frame
   helm-echo-input-in-header-line t
   helm-etags-fuzzy-match t
   helm-ff-candidate-number-limit 100
   helm-ff-file-name-history-use-recentf t
   helm-ff-guess-ffap-filenames t
   helm-ff-guess-ffap-urls t
   helm-ff-history-max-length 1000
   helm-ff-search-library-in-sexp t
   helm-ff-skip-boring-files t
   helm-ff-tramp-not-fancy nil
   helm-file-cache-fuzzy-match t
   helm-findutils-search-full-path t
   helm-follow-mode-persistent t
   helm-grep-file-path-style 'relative
   helm-grep-max-length-history 10000
   helm-grep-default-command "ack-grep -Hn --color --smart-case --no-group %e %p %f"
   helm-grep-default-recurse-command "ack-grep -H --color --smart-case --no-group %e %p %f"
   helm-home-url "https://theunixman.com/"
   helm-imenu-fuzzy-match    t
   helm-input-method-verbose-flag 'complex-only
   helm-lisp-fuzzy-completion t
   helm-locate-fuzzy-match t
   helm-migemo-mode nil
   helm-mini-default-sources '(helm-source-buffers-list
                               helm-source-recentf
                               helm-source-buffer-not-found)
   helm-moccur-auto-update-on-resume 'noask
   helm-mode-fuzzy-match t
   helm-move-to-line-cycle-in-source     t
   helm-net-prefer-curl t
   helm-recentf-fuzzy-match t
   helm-save-configuration-functions '(set-frame-configuration . current-frame-configuration)
   helm-scroll-amount 8
   helm-semantic-fuzzy-match t
   helm-session-fuzzy-match t
   helm-show-completion-default-display-function #'helm-display-buffer-in-own-frame
   helm-swoop-use-fuzzy-match t
   helm-swoop-use-line-number-face t
   helm-M-x-always-save-history t
   helm-M-x-requires-pattern nil
   helm-M-x-fuzzy-match t
   helm-window-configuration-hook (lambda ()
                                    (with-helm-buffer
                                      (linum-mode nil)
                                      (modify-frame-parameters
                                       nil
                                       '((user-position . t)
                                         (left . (- 0))
                                         (top . (/ (display-pixel-height) 3))
                                         )
                                       )
                                      )
                                    )
   )
  :commands (helm-M-x helm-command-prefix helm-display-buffer-in-own-frame)
  :bind (("M-x" . helm-M-x)
         ("M-h" . helm-command-prefix)
         ("C-x r b" . helm-filtered-bookmarks)
         ("C-x C-f" . helm-find-files)
         ("M-y" . helm-show-kill-ring)
         ("C-x b" . helm-mini)
         ("C-c o" . helm-occur)
         ("C-h a" . helm-apropos)
         ("C-x C-r" . helm-recentf)
         :map helm-command-map
         ("C-;" . ace-jump-helm-line)
         ("M-i" . helm-info-at-point)
         ("M-a" . helm-apt)
         ("M-b" . helm-bibtex)
         ("M-m" . math-symbols-helm)
         ("y" . helm-yas-complete)
         )

  :config
  (add-to-list 'helm-grep-ignored-directories "dist/")
  (add-to-list 'helm-grep-ignored-directories "dist-new/")
  (add-to-list 'helm-grep-ignored-directories "*cabal*sandbox/")
  (add-to-list 'helm-grep-ignored-files "*.hi")
  (helm-mode)
  (helm-adaptive-mode)
  (helm-flx-mode)
  (helm-fuzzier-mode)
  (helm-popup-tip-mode)
  )

(use-package helm-adaptive
  :ensure helm
  :init
  (setq helm-adaptive-history-file (concat
                                    (emacs-run-data-dir "helm" "adaptive-history")
                                    "helm-adaptive-history")
        helm-adaptive-history-length 5000
        )
  :commands (helm-adaptive-mode)
  )

(use-package helm-ring
  :ensure helm
  :init (setq helm-register-max-offset 1024)
  :commands (helm-mark-ring
             helm-global-mark-ring
             helm-all-mark-rings helm-register
             helm-execute-kmacro)
  )

(use-package helm-apt
  :ensure helm
  :commands helm-apt
  )

(use-package helm-bookmark
  :ensure helm
  :commands (helm-bookmarks helm-filtered-bookmarks)
  )

(use-package helm-utils
  :ensure helm
  :commands (helm-popup-tip-mode)
  )

(use-package helm-sys
  :ensure helm
  :commands (helm-top helm-list-emacs-process helm-xrandr-set)
  )

(use-package helm-proc
    :commands helm-proc
    :ensure t)

(use-package helm-gitignore
  :commands helm-gitignore
  :ensure t)


;; helm swoop

(use-package helm-swoop
    :ensure t
    :commands helm-swoop-from-isearch
    :bind
    (:map isearch-mode-map
          ("M-i" . helm-swoop-from-isearch))
    )

(use-package helm-descbinds
  :ensure t
  :commands helm-descbinds
  )

(use-package helm-package
  :ensure t
  :commands helm-package
  )

(use-package helm-themes
  :ensure t
  :commands helm-themes
  )

(use-package ace-jump-helm-line
  :ensure t
  :commands ace-jump-helm-line
  )

(use-package helm-descbinds
  :ensure t
  :commands helm-descbinds
    )

(use-package helm-package
  :ensure t
  :commands helm-package
  )

(use-package helm-themes
  :ensure t
  :commands helm-themes
  )

(use-package helm-fuzzier
  :ensure t
  :init
  (setq
   helm-fuzzier-max-query-len 15
   helm-fuzzier-preferred-max-group-length 5
   )
  :commands (helm-fuzzier-mode)
  )

(use-package helm-config
  :ensure helm
  )

(use-package helm-bibtex
  :ensure t
  :init
  (setq bibtex-completion-format-citation-functions
        '((org-mode      . bibtex-completion-format-citation-org-link-to-PDF)
          (latex-mode    . bibtex-completion-format-citation-cite)
          (markdown-mode . bibtex-completion-format-citation-pandoc-citeproc)
          (default       . bibtex-completion-format-citation-default))
        )
  :commands (helm-bibtex helm-bibtex-with-local-bibliography)
  )

(use-package helm-ag
  :ensure t
  :commands (helm-ag helm-ag-this-file helm-ag-buffers helm-ag-project-root)
  )

(use-package helm-flx
  :ensure t
  :commands helm-flx-mode
  )

(use-package all-ext
  :ensure t)

(use-package math-symbols
  :ensure t
  :init

  ;; Prepare the global keymap for `math-symbols'
  (defvar math-symbols-global-keys (make-sparse-keymap)
    "The global keymap for `math-symbols'.")

  (define-prefix-command 'math-symbols-global-keys)

  :commands
  (math-symbols-helm
   math-symbols-from-tex-region
   math-symbols-insert
   math-symbols-to-tex-region
   math-symbols-to-tex-unicode-region
   math-symbols-italic
   math-symbols-italic-region
   math-symbols-fraktur-region
   math-symbols-script-region
   math-symbols-double-struck-region
   math-symbols-superscript-region)
  :bind
  (("C-c m" . math-symbols-global-keys)
   :map math-symbols-global-keys
   ("t" . math-symbols-from-tex-region)
   ("T" . math-symbols-to-tex-region)
   ("i" . math-symbols-insert)
   ("u" . math-symbols-to-tex-unicode-region)
   ("I" . math-symbols-italic-region)
   ("F" . math-symbols-fraktur-region)
   ("S" . math-symbols-script-region)
   ("D" . math-symbols-double-struck-region)
   ("^" . math-symbols-superscript-region)
   )
  )

(use-package helm-cscope
  :ensure t
  :init
  (add-hook 'c-mode-common-hook #'helm-cscope-mode)
  :commands (helm-cscope-mode)
  )

(use-package helm-color
  :ensure helm
  :commands (helm-colors)
  )

(use-package helm-font
  :ensure helm
  :commands (helm-select-xfont helm-ucs))

(provide 'init-helm)
