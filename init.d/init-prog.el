;;; init-prog.el --- Configure programming modes.    -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: languages, local, languages, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'use-package)

(use-package form-feed
  :ensure t
  :commands (form-feed-mode)
  :config
  (add-hook 'prog-mode-hook #'form-feed-mode)

  )

(use-package yaml-mode
  :ensure t
  :init
  (add-hook 'yaml-mode-hook '(lambda () (aggressive-fill-paragraph-mode (-1))))
  )

(use-package mustache
  :ensure t)

(use-package apu
  :ensure t
  )

(use-package ggtags
  :ensure t
  :init
  (setq ggtags-process-environment t
        ggtags-auto-jump-to-match 'first
        ggtags-global-window-height 16
        ggtags-global-abbreviate-filename 130
        ggtags-completion-cache (emacs-run-data-dir "ggtags" "completion-cache")
        ggtags-eldoc-cache (emacs-run-data-dir "ggtags" "eldoc-cache")
        ggtags-global-history-length 16384
        ggtags-global-ignore-case t
        ggtags-sort-by-nearness t)
  :commands (ggtags-navigation-mode)
  )

;; (use-package python-mode
;;   :ensure t
;;   :commands (python-mode)
;;   )

;; (use-package jedi-core
;;   :ensure t
;;   :commands (jedi:setup jedi:install-server)
;;   :init
;;   (add-hook 'python-mode-hook '(lambda () (jedi:install-server) (jedi:setup)))
;;   (setq jedi:complete-on-dot t
;;         jedi:tooltip-method 'pos-tip
;;         jedi:use-shortcuts t)
;;   )

;; (use-package jedi
;;   :ensure t
;;   :commands (jedi:ac-setup)
;;   )

(use-package python
  :ensure t
  )

(use-package hcl-mode
  :ensure t)

(use-package header2
  :ensure t)

(use-package apache-mode
  :ensure t)

(use-package lentic
  :ensure t
  :init
  (setq lentic-mode-line-lighter "L")
  :commands (lentic-mode
             lentic-default-init
             lentic-init
             easy-menu-change
             global-lentic-mode)
  :config
  (global-lentic-mode)
  :demand
  )

(use-package strace-mode
  :ensure t)

(use-package lua-mode
  :ensure t)

(use-package cmake-mode
  :ensure t
  :commands (cmake-mode cmake-command-run cmake-help-list-commands)
  )

(use-package cmake-ide
  :ensure t
  :demand
  :commands (cmake-ide-setup)
  :config (cmake-ide-setup)
  )

(use-package syntax-subword
  :ensure t
  :commands (syntax-subword-mode global-syntax-subword-mode)
  :config
  (global-syntax-subword-mode)
  :demand
  )

;; (use-package autovirtualenv
;;   :ensure t
;;   :init
;;   (add-hook 'projectile-after-switch-project-hook #'auto-virtualenv-set-virtualenv)
;;   (add-hook 'python-mode-hook #'auto-virtualenv-set-virtualenv)

;;   :commands (auto-virtualenv-set-virtualenv)
;;   )

(provide 'init-prog)
;;; init-prog.el ends here
