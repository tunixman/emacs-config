;;; init-clang.el --- Initialize various clang packages.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: c, languages, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize `company-clang'.

;;; Code:

(require 'use-package)

(use-package company-clang
  :ensure t
  :commands company-clang
  :init
  (setq company-clang-executable "/opt/rocm/hcc/bin/clang")
  )

(use-package clang-format
  :ensure t
  :commands (clang-format clang-format-region clang-format-buffer)
  :init
  (setq clang-format-executable "/opt/rocm/hcc/bin/clang-format")
  :bind (:map c-mode-map
              ("M-\\" . clang-format-region)
              ("M-C-\\" . clang-format-buffer)
             )
  )

(use-package flycheck-clang-tidy
  :ensure t
  :commands flycheck-clang-tidy-setup
  :init
  (add-hook 'flycheck-mode-hook #'flycheck-clang-tidy-setup)
  )

(provide 'init-clang)
;;; init-clang.el ends here
