(require 'use-package)

;; single dired

(use-package dired
  :commands dired
  :after (diff-hl diff-hl helm)
  :bind (:map dired-mode-map
              ("M-i" . helm-swoop)
              ("M-RET" . dired-find-file-other-window)
              )
  :init
  (add-hook 'dired-mode #'auto-revert-mode)
  (add-hook 'dired-mode #'diff-hl-dired-mode)
  )

(use-package diff-hl
  :commands diff-hl-dired-mode
  :ensure t
  )
(use-package dired-rainbow
  :ensure t)

(use-package dired-open
  :ensure t)

(use-package dired-subtree
  :bind (:map dired-mode-map
              ("i" . dired-subtree-toggle)
              )
  :ensure t
  )

(use-package dired+
  :ensure t)

(use-package dired-sort-menu+
 :ensure t)

(provide 'init-dired)
