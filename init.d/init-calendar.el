;;; init-calendar.el --- Initialize the various calendar integrations.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: calendar, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize calfw and its various integrations.

;;; Code:

(require 'use-package)

(defun open-calendar-buffer ()
  "Opens the calfw buffer with our sources."
  (interactive)

  (cfw:open-calendar-buffer
   :contents-sources
   (list
    (cfw:org-create-source "SaddleBrown")
    (cfw:cal-create-source "Deepskyblue4")
    (cfw:ical-create-source
     "Meetings" "~/Documents/Calendars/Meetings.ics" "Orange3")
    (cfw:ical-create-source
     "ToDos" "~/Documents/Calendars/ToDos.ics" "Firebrick4")
    )
   )
  )

(use-package calfw
  :ensure t
  :commands (cfw:open-calendar-buffer)
  :bind (("<M-XF86HomePage>" . open-calendar-buffer))
  )

(use-package calfw-org
  :ensure t
  :init
  (setq cfw:org-overwrite-default-keybinding t)
  :commands (cfw:org-create-source cfw:open-org-calendar)
  )

(use-package calfw-cal
  :ensure t
  :commands cfw:cal-create-source
  )

(use-package calfw-ical
  :ensure t
  :commands cfw:ical-create-source
  )

(use-package calendar
  :pin manual
  :commands (diary-include-other-diary-files diary-sort-entries)
  :init
  (setq calendar-mark-diary-entries-flag t
        calendar-setup 'two-frames
        diary-abbreviated-year-flag nil
        )
  (add-hook 'diary-list-entries-hook #'diary-include-other-diary-files)
  (add-hook 'diary-list-entries-hook #'diary-sort-entries)
  :config
  (setq calendar-time-display-form '(24-hours
                                     ":" minutes
                                     (if time-zone " (")
                                     time-zone
                                     (if time-zone ")")
                                     )
        )
  )

(provide 'init-calendar)
;;; init-calendar.el ends here
