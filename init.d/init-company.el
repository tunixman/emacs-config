(require 'use-package)

(use-package company
  :ensure t
  :init
  (setq
   company-idle-delay 0.25
   company-show-numbers nil
   company-minimum-prefix-length 3)

  :config
  (add-to-list 'company-backends #'company-dict)
  (add-to-list 'company-backends #'company-latex-commands)
  (add-to-list 'company-backends #'company-math-symbols-latex)
  (add-to-list 'company-backends #'company-math-symbols-unicode)
  (add-to-list 'company-backends #'company-cabal)
  (add-to-list 'company-backends #'company-shell)
  (add-to-list 'company-backends #'company-web-html)
  (add-to-list 'company-backends #'company-web-jade)
  (add-to-list 'company-backends #'company-web-slim)
  (add-to-list 'company-backends #'company-bibtex)
  (add-to-list 'company-backends #'company-clang)
  (define-key company-mode-map (kbd "M-p") 'helm-company)
  (define-key company-active-map (kbd "M-p") 'helm-company)
  (global-company-mode)
  (company-flx-mode)
  (company-statistics-mode)
  (company-auctex-init)
  )

(use-package company-statistics
  :ensure t
  :init (setq company-statistics-file
              (concat
               (emacs-run-data-dir "company" "statistics")
               "cache.el"))
  :commands (company-statistics-mode)
  )

(use-package helm-company
  :ensure t
  :init (setq helm-company-fuzzy-match t)
  :commands (helm-company)
  )

(use-package company-flx
  :ensure t
  :commands company-flx-mode
  )

(use-package company-cabal
  :commands company-cabal
  :ensure t)

(use-package company-ghci
  :commands company-ghci
  :ensure t)

(use-package company-math
  :ensure t
  :commands (company-latex-commands
             company-math-symbols-latex
             company-math-symbols-unicode)
  )

(use-package company-auctex
  :ensure t
  :commands (company-auctex-init)
  )

(use-package company-shell
  :ensure t
  :commands company-shell
  )

(use-package company-web
  :ensure t
  :commands (company-web-html company-web-jade company-web-slim)
  )

(use-package company-dict
  :ensure t
  :init
  (setq company-dict-enable-fuzzy t)
  :commands (company-dict)
  )

(use-package company-bibtex
  :ensure t
  :commands (company-bibtex)
  )

(provide 'init-company)
