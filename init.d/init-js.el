(require 'use-package)

;; js2 mode

(use-package js2-mode
  :ensure t
  :init
  (add-hook 'js2-mode-hook #'js2-refactor-mode)
  (setq js2-mode-assume-strict t
        js2-mode-indent-ignore-first-tab t
        js2-dynamic-idle-timer-adjust 15000
        )
  :mode "\\.js\\'"
  )

(use-package js2-refactor
  :ensure t
  :init
  (setq js2r-use-strict t
        js2r-prefered-quote-type 2
        js2r-always-insert-parens-around-arrow-function-params t
        )
  :commands (js2-refactor-mode)
  )

(use-package json-mode
  :ensure t
  )

(provide 'init-js)
