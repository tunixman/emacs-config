;;; init-misc.el --- Initialize miscellaneous things.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky(require 'use-package) <evan@theunixman.com>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize things that don't fit anywhere else.

;;; Code:
(require 'use-package)

(use-package info
  :config
  (add-to-list 'Info-additional-directory-list (expand-file-name "~/.emacs.d/org.d/info/dir"))
  )

(use-package desktop+
  :init
  (setq desktop+-base-dir (concat (emacs-run-data-dir "desktop+" "sessions")))
  :bind
  (("M-s c" . desktop+-create)
   ("M-s l" . desktop+-load))
  :commands (desktop+-create
             desktop+-load
             desktop+-create-auto
             desktop+-load-auto)
  )

(use-package savehist
  :commands (savehist-mode)
  :config
  (savehist-mode)
  :demand
  )

(use-package files
  :init (setq save-abbrevs 'silently)
  )

(use-package abbrev
  :init (setq abbrev-file-name (concat (emacs-run-data-dir "abbrev" "abbrev") "abbrevs"))
  )

(use-package undo-tree
  :ensure t
  :demand
  :commands (global-undo-tree-mode)
  :config (global-undo-tree-mode)
  :init
  (setq
   undo-tree-visualizer-timestamps t
   undo-tree-visualizer-diff t
   undo-tree-enable-undo-in-region t
   )
  )

(use-package paren
  :init (setq show-paren-style 'mixed)
  :commands (show-paren-mode)
  :config (show-paren-mode)
  :demand
  )

(use-package calendar
  :init
  (setq
   diary-file (concat (emacs-run-data-dir "calendar" "diary") "diary")
   )
  )

(use-package recentf
  :ensure t
  :init (setq
         recentf-save-file (concat (emacs-run-data-dir "recentf" "recent-save") "recentf"))
  :demand
  )

(use-package bookmark
  :init
  (setq
   bookmark-file (concat (emacs-run-data-dir "bookmark" "file") "bookmark.el")
   bookmark-default-file bookmark-file
   bookmark-save-flag 1
   bkmp-last-as-first-bookmark-file nil)
  )

(use-package bookmark+
  :ensure t
  :demand
  )

(use-package systemd
  :ensure t)

(use-package irfc
  :ensure t
  :commands (irfc-visit irfc-follow irfc-reference-goto)
  )

(use-package edit-indirect
  :ensure t
  :commands (edit-indirect-region)
  )

(use-package edit-indirect-region-latex
  :ensure t
  :commands (edit-indirect-latex edit-indirect-region-latex)
  )

(use-package ediff
  :init
  (setq
   ediff-make-buffers-readonly-at-startup nil
   ediff-merge-split-window-function 'split-window-vertically
   ediff-shell "zsh"
   ediff-use-long-help-message t
   ediff-window-setup-function #'ediff-setup-windows-multiframe
   ediff-ignore-similar-regionres t
   )
  )

(use-package powerline
  :ensure t
  :init
  (setq
   powerline-text-scale-factor 1.0
   powerline-gui-use-vcs-glyph t
   powerline-display-buffer-size nil
   powerline-display-mule-info t
   powerline-narrowed-indicator "⇅"
   )
  :commands (powerline-default-theme)
  )

(use-package diminish
  :ensure t
  :demand
  :commands (diminish)
  :config
  (diminish 'emacs-lisp " elsp ")
  (diminish 'company-mode " C ")
  )

(use-package nlinum
  :ensure t
  :demand
  :commands global-nlinum-mode
  :config
  (global-nlinum-mode)
  )

(use-package url-queue
  :init
  (setq url-queue-parallel-processes 24
        url-queue-timeout 30)
  )

(provide 'init-misc)
;;; init-misc.el ends here
