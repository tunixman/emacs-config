(require 'use-package)

(use-package ghc
  :ensure t
  :pin manual
  :commands (ghc-init ghc-debug)
  )

(use-package company-ghc
  :ensure t
  :commands (company-ghc)
  )

(use-package company-cabal
  :ensure t
  :commands (company-cabal)
  )

(defun tum-haskell-mode-hook-func ()
  "Set up `haskell-mode' for misandry."

  (set (make-local-variable 'company-backends)
       (append '((company-capf company-dabbrev-code))
               company-backends))

  (haskell-decl-scan-mode)
  (highlight-uses-mode)
  (imenu-add-menubar-index)
  (haskell-indentation-mode)
  ;; (turn-on-tum-haskell-unicode-input-method)
  )

(defun tum-haskell-speedbar-extensions-hook ()
  "Add the right extensions for Haskell to `speedbar'."

  (mapc #'speedbar-add-supported-extension
        '(".hs" ".lhs"
          ".cabal"
          "cabal.project" "cabal.project.local"
          "cabal.sandbox.config"))
  )

(use-package haskell-mode
    :ensure t
    :commands (haskell-mode haskell-cabal-mode)
    :mode
    ("\\.lhs$" . haskell-mode)
    ("\\.hs$" . haskell-mode)
    (".xmobarrc$" . haskell-mode)
    ("\\.cabal$" . haskell-cabal-mode)
    ("cabal.project" . haskell-cabal-mode)
    ("cabal.project.local" . haskell-cabal-mode)

    :init
    (setq
        haskell-completing-read-function 'helm--completing-read-default
        haskell-doc-prettify-types t
        haskell-indentation-electric-flag t
        haskell-indentation-layout-offset 4
        haskell-indentation-left-offset 4
        haskell-indentation-starter-offset 4
        haskell-indentation-where-pre-offset 4
        haskell-indentation-where-post-offset 4
        haskell-interactive-types-for-show-ambiguous t
        haskell-notify-p t
        haskell-process-auto-import-loaded-modules t
        haskell-process-log t
        haskell-process-suggest-hoogle-imports t
        haskell-process-suggest-remove-import-lines t
        haskell-process-use-presentation-mode t
        haskell-interactive-mode-scroll-to-bottom nil
        haskell-mode-hook #'tum-haskell-mode-hook-func
        )

    (add-hook 'speedbar-load-hook #'tum-haskell-speedbar-extensions-hook)

    :bind (:map haskell-mode-map
                ("C-`" . haskell-interactive-bring)
                ("M-." . haskell-mode-jump-to-def-or-tag)
                ("C-c C-c" . haskell-compile)
                :map haskell-cabal-mode-map
                ("C-c C-c" . haskell-compile))
    )

(provide 'init-haskell)
