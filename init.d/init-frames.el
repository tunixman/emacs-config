;;; init-frames.el --- Configure frame commands.     -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Reassign some basic keys for frames instead of terminals.

;;; Code:

(require 'use-package)

(use-package tum-frames
  :ensure t
  :pin manual
  :commands (tum-frames-mode)
  :config
  (tum-frames-mode)
  )

(use-package pos-tip
  :ensure t
  :init (setq
         pos-tip-border-width 2
         pos-tip-internal-border-width 3
         pos-tip-tab-width 16
         pos-tip-use-relative-coordinates t)
  :demand
  )

;; (use-package speedbar
;;   :init
;;   (setq speedbar-use-imenu-flag t
;;         speedbar-default-position 'right
;;         speedbar-tag-hierarchy-method #'speedbar-sort-tag-hierarchy
;;         speedbar-directory-button-trim-method 'trim
;;         speedbar-vc-do-check nil
;;         speedbar-default-position 'right
;;         speedbar-frame-parameters '((minibuffer)
;;                                     (width . 200)
;;                                     (height . (* 0.85 (display-pixel-height)))
;;                                     (user-size . t)
;;                                     (user-position . t)
;;                                     (border-width)
;;                                     (unsplittable . t))
;;         speedbar-indentation-width 2
;;         speedbar-track-mouse-flag t)

;;   :commands (speedbar-mode speedbar-frame-mode)
;;   )

(use-package sr-speedbar
  :ensure t
  :bind (("M-<f2>" . sr-speedbar-toggle))
  :init (setq sr-speedbar-default-width 80
              sr-speedbar-max-width 140)
  :commands (sr-speedbar-toggle sr-speedbar-open)
  )

(provide 'init-frames)
;;; init-frames.el ends here
