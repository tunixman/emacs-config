;;; init-yasnippet.el --- Configuration for yasnippet.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: abbrev, local, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Ensure yasnippet is installed and loaded.

;;; Code:

(require 'use-package)

(use-package haskell-snippets
  :config
  (yas-recompile-all)
  (yas-reload-all)
  :ensure t
  )

(use-package helm-c-yasnippet
  :ensure t
  :init
  (setq helm-yas-space-match-any-greedy t
        helm-yas-display-key-on-candidate t
        yas-triggers-in-field t)
  :commands helm-yas-complete
  :config
  (yas-global-mode 1)
  )

(use-package yasnippet
  :commands (yas-global-mode
             yas-expand
             yas-load-directory
             yas-activate-extra-mode
             yas-insert-snippet
             yas-visit-snippet-file
             yas-new-snippet
             yas-load-snippet-buffer
             yas-tryout-snippet
             yas-describe-tables)
  :config
  (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets/")
  (yas-global-mode)
  )

(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
