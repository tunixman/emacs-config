;;; init-environ.el --- Environment initialization.
;;; -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize the execution environment for subprocesses.

;;; Code:

(require 'use-package)

(use-package exec-path-from-shell
  :ensure t
  :commands (exec-path-from-shell-initialize exec-path-from-shell-variables)
  )

(use-package direnv
  :ensure t
  :commands (direnv-mode direnv-envrc-mode)
  )

(provide 'init-environ)
;;; init-environ.el ends here
