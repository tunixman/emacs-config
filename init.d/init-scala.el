;;; init-scala.el --- Initialize scala-mode and ensime.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Evan Cofsky

;; Author: Evan Cofsky <evan@theunixman.com>
;; Keywords: languages, processes, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This initializes the scala-mode and the ENSIME packages.

;;; Code:

(require 'use-package)

(use-package scala-mode
  :init
  (setq scala-indent:align-forms t
        scala-indent:align-parameters t
        scala-indent:indent-value-expression t
        scala-indent:step 2)
  :ensure t
  )

(use-package ensime
  :ensure t
  :init
  (setq ensime-auto-connect t
        ensime-auto-generate-config t
        ensime-outline-mode-in-events-buffer t
        ensime-startup-notification nil
        ensime-startup-snapshot-notification nil
        ensime-default-java-flags '("-Xmx256m" "-Xss4M -XX:+AggressiveHeap -XX:CMSTriggerRatio=50 -XX:MaxMetaspaceSize=128M -XX:+UseConcMarkSweepGC")
        ensime-eldoc-hints t
        ensime-graphical-tooltips t
        ensime-overlays-use-font-lock t
        ensime-search-interface 'helm
        )
  )

(provide 'init-scala)
;;; init-scala.el ends here
